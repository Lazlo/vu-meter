#!/bin/bash

pkgs=""
pkgs="$pkgs make python3 python3-pip"

apt-get -q install -y $pkgs

pip3 install --system -r $(dirname $0)/requirements.txt
