# Digital Volume Unit Meter

A digital version of the analog [VU meter](https://en.wikipedia.org/wiki/VU_meter).

It measures the audio volume from its analog audio input and provides the
digital result to the user interface e.g. a dynamic bar graph.

![block-diagram](doc/diagrams/block-diagram.svg)

## Building

```
sudo ./setup.sh
make docs
```
