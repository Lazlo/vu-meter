class View():

	def __init__(self, x, y, width, height):
		self.x = x
		self.y = y
		self.max_height = height
		self.width = width
		self.height = height

	def scaleheight(self, percent):
		self.height = int((self.max_height / 100) * percent)

	@property
	def rect(self):
		r = (self.x, self.y, self.width, self.height)
		return r

	@property
	def rect_inverse(self):
		r = (self.x, self.max_height - self.height, self.width, self.height)
		return r
