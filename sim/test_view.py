import unittest
from view import View

class ViewInitTestCase(unittest.TestCase):

	def setUp(self):
		self.expected_x = 0
		self.expected_y = 0
		self.expected_width = 1
		self.expected_height = 10
		self.v = View(self.expected_x, self.expected_y,
				self.expected_width, self.expected_height)

	def testSetsX(self):
		self.assertEqual(self.expected_x, self.v.x)

	def testSetsY(self):
		self.assertEqual(self.expected_y, self.v.y)

	def testSetsWidth(self):
		self.assertEqual(self.expected_width, self.v.width)

	def testSetsHeight(self):
		self.assertEqual(self.expected_height, self.v.height)

class ViewRectTestCase(unittest.TestCase):

	def setUp(self):
		self.expected_x = 0
		self.expected_y = 0
		self.expected_width = 1
		self.expected_height = 10
		self.v = View(self.expected_x, self.expected_y,
				self.expected_width, self.expected_height)

	def testGetRect(self):
		expected = (self.expected_x, self.expected_y,
				self.expected_width, self.expected_height)
		self.assertEqual(self.v.rect, expected)

	def testGetRectInverse(self):
		expected = (self.expected_x, 5,
				self.expected_width, 5)
		self.v.scaleheight(50)
		self.assertEqual(self.v.rect_inverse, expected)

class ViewScaleHeightTestCase(unittest.TestCase):

	def setUp(self):
		self.expected_x = 0
		self.expected_y = 0
		self.expected_width = 1
		self.expected_height = 10
		self.v = View(self.expected_x, self.expected_y,
				self.expected_width, self.expected_height)

	def testHalf(self):
		percent = 50
		self.v.scaleheight(percent)
		self.assertEqual(self.v.height, 5)

	def testFourth(self):
		percent = 25
		self.v.scaleheight(percent)
		self.assertEqual(self.v.height, 2)

	def testNineTenth(self):
		percent = 90
		self.v.scaleheight(percent)
		self.assertEqual(self.v.height, 9)

	def testRepeatedCallsUseOriginalHeight(self):
		"""
		We will test that repeated calls to scaleheight()
		will use the original 'height' value passed on creation
		of the object. Otherwise every re-scaling operation
		would work on the height that was previously set, which
		is not the expected behavior for our view.
		"""
		self.v.scaleheight(0)
		self.v.scaleheight(100)
		self.assertEqual(self.v.height, 10)
