class Model():

	def __init__(self):
		self.percent = 0

	@property
	def percent(self):
		return self.__percent

	@percent.setter
	def percent(self, percent):
		if percent < 0 or percent > 100:
			return
		self.__percent = percent
