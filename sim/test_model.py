import unittest
from model import Model

class ModelSetPercentTestCase(unittest.TestCase):

	def setUp(self):
		self.m = Model()

	def testSetPercent(self):
		expected = 5
		self.m.percent = expected
		self.assertEqual(self.m.percent, expected)

	def testSetPercentFailsWithNegativeValue(self):
		expected = 100
		self.m.percent = expected
		self.m.percent = -1
		self.assertEqual(self.m.percent, expected)

	def testSetPercentFailsWithPlus100Value(self):
		expected = 30
		self.m.percent = expected
		self.m.percent = 500
		self.assertEqual(self.m.percent, expected)
